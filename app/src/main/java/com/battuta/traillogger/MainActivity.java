package com.battuta.traillogger;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class MainActivity extends AppCompatActivity implements
                     NavigationView.OnNavigationItemSelectedListener {
    public static final String TAG = "MainActivity";
    private DrawerLayout mDrawerLayout;
    private NavigationView mLeftDrawer;
    private Toolbar mToolbar;
    private String mTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.activity_main_content_frame, new FrontPageFragment())
                .commit();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.activity_main_drawer_layout);
        mLeftDrawer = (NavigationView) findViewById(R.id.activity_main_left_drawer);
        mToolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);

        //set toolbar as action bar
        setSupportActionBar(mToolbar);

        //Create action bar toggle & override method when drawer is opened
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                mToolbar, R.string.open_drawer, R.string.close_drawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        mDrawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();


        // Use Navigation View Method Inflate to Add a XML Menu
        mLeftDrawer.inflateMenu(R.menu.navigation_drawer_menu);

        // Use Navigation Listener on Menu Items
        mLeftDrawer.setNavigationItemSelectedListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handles Navigation between fragments when item menu is clicked
        int id = item.getItemId();
        changeFragment(id);
        mDrawerLayout.closeDrawers();
        setActionBarTitle(id);
        return false;


    }

    private void changeFragment(int id) {
        Fragment displayFragment = null;

        switch (id) {
            case R.id.nav_home:
                displayFragment = new FrontPageFragment();
                break;
            case R.id.nav_TrailMainList:
                displayFragment = new TrailMainListFragment();
                break;
            case R.id.nav_Faq:
                displayFragment = new FaqFragment();
                break;
        }

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_content_frame, displayFragment)
                .addToBackStack(displayFragment.getTag())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

    private void setActionBarTitle(int id) {
        // Method to change the title of the toolbar based on id name
        switch (id) {
            case R.id.nav_home:
                mTitle = getResources().getString(R.string.app_name);
                break;
            case R.id.nav_TrailMainList:
                mTitle = getResources().getString(R.string.title_trail_main_list);
                break;
            case R.id.nav_Faq:
                mTitle = getResources().getString(R.string.title_FAQ);
                break;
            default:
                mTitle = getResources().getString(R.string.app_name);
                break;
        }

        mToolbar.setTitle(mTitle);
    }

}
